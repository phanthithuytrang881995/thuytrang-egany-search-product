import React, { Component } from 'react'

export default class ProductItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: this.props.product,
            isDiscount: false,
            discountPercent: null
        };
    }

    checkStock() {
        if (this.state.product.variants[0].inventory_management === null) {
            return (<b>{this.state.product.variants[0].inventory_quantity}</b>)
        } else {
            if (this.state.product.variants[0].inventory_quantity <= 0) {
                return (<span className="text-danger">Hết hàng</span>)
            }
        }
    }

    calDiscountPercentage() {
        if (this.state.product.variants[0].price === this.state.product.variants[0].compare_at_price) {
            return "";
        } else {
            return (<span className="badge badge-warning">- {100 - parseInt(this.state.product.variants[0].price / this.state.product.variants[0].compare_at_price * 100)}% </span>)
        }
    }
    checkAllowBuy() {
        if (this.state.product.variants[0].inventory_management === null) {
            return <button className="btn btn-primary btn-block">Mua ngay</button>;
        } else {
            if (this.state.product.variants[0].inventory_quantity <= 0) {
                if (this.state.product.variants[0].inventory_policy === "allow") {
                    return <button className="btn btn-primary btn-block">Đặt hàng trước</button>;
                }else{
                    return "";
                }
            }else{
                return <button className="btn btn-primary btn-block">Mua ngay</button>;
            }
             
        }
    }
    checkInventoryManagement(){
        if (this.state.product.variants[0].inventory_management === null) {
            return <p>Mua hàng bất kể số lượng</p>;
        }else{
            return <p>Manual</p>;
        }
    }
    render() {
        return (
            <div className="card mb-3">
                <img className="card-img-top" src={this.state.product.image.src} alt={this.state.product.title} />
                <div className="card-body">
                    <h5 className=" h6 card-title text-uppercase">{this.state.product.title} {this.calDiscountPercentage()}</h5>
                    <p>Mã sản phẩm: <span className="text-muted">{this.state.product.variants[0].sku}</span></p>
                    <p>Số lượng: {this.checkStock()}</p>
                    {this.checkInventoryManagement()} 
                    <h6 className="mb-3">Price: <span className="text-danger mr-2">{this.state.product.variants[0].price}đ</span> <del className="text-muted"> {this.state.product.variants[0].compare_at_price}đ</del></h6>
                    {this.checkAllowBuy()}
                </div>
            </div>
        )
    }
}
