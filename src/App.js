import React, { Component } from "react";
import './App.css';
import Header from './components/Header.js'
import ProductItem from './components/ProductItem.js'
import data_json from "./data.json"

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: data_json.products,
      search_value: '',
      isSearch: false,
      searchProducts: []
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleRefresh = this.handleRefresh.bind(this);
    this.showData = this.showData.bind(this);
  }

  handleChange(e) {
    console.log(e.target.value);
    this.setState({
      isSearch: true,
      search_value: e.target.value
    })

    let filteredProducts = [];

    if (e.target.value !== "") {
      filteredProducts = this.state.products.filter(product => {
        const lc = product.title.toLowerCase();
        const filter = e.target.value.toLowerCase();
        return lc.includes(filter);
      });
    } else {
      filteredProducts = this.state.products;
    }

    this.setState({
      searchProducts: filteredProducts
    });
  }

  handleRefresh() {
    console.log('Refresh');
    this.setState({
      isSearch: false,
      search_value: "",
    })

  }

  showData() {
    let data = []
    if (this.state.isSearch === false) {
      data = this.state.products;
      console.log("chưa search");
    } else {
      data = this.state.searchProducts;
      console.log("đã search");
    }
    if (data.length === 0) {
      return <h2 className="text-muted text-center">Không tìm thấy kết quả nào!</h2>;
    } else {
      return (
        <div className="row">
          {data.map((product) => {
          return (

          <div className="col-6 col-md-3">
            <ProductItem key={product.id} product={product}></ProductItem>
          </div>

          );
        })}
        </div >
      )
    }
  }

  render() {
    return (
      <div className="App">``
        <Header></Header>
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-12 col-lg-6">
              <div className="input-group mb-4">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Tìm kiếm theo tên" value={this.state.search_value}
                  onChange={(e) => this.handleChange(e)} />
                <div className="input-group-append">
                  <button className="btn btn-secondary" type="button" onClick={this.handleRefresh}> Refresh </button>
                </div>
              </div>
            </div>
          </div>

          <hr></hr>
          <div className="main">
            {this.showData()}
          </div>
        </div>
      </div>
    )
  }
}

